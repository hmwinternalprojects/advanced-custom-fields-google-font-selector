<?php

/*
Plugin Name: Advanced Custom Fields: Google Font Selector
Plugin URI: https://github.com/danielpataki/ACF-Google-Font-Selector
Description: A field for Advanced Custom Fields which allows users to select Google fonts with advanced options
Version: 3.1
Author: Daniel Pataki
Author URI: http://danielpataki.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Bitbucket Plugin URI: https://bitbucket.org/hmwinternalprojects/advanced-custom-fields-google-font-selector
*/

// Include Common Functions
include_once('functions.php');


add_action('plugins_loaded', 'acfgfs_load_textdomain');
/**
 * Load Text Domain
 *
 * Loads the textdomain for translations
 *
 * @author Daniel Pataki
 * @since 3.0.0
 *
 */
function acfgfs_load_textdomain() {
	load_plugin_textdomain( 'acf-google-font-selector-field', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
}

add_action('admin_init', 'acfgfs_register_settings');

function acfgfs_register_settings() {
  register_setting('acfgfs', 'google_api_key');
}


add_action('acf/include_field_types', 'include_field_types_google_font_selector');
/**
 * ACF 5 Field
 *
 * Loads the field for ACF 5
 *
 * @author Daniel Pataki
 * @since 3.0.0
 *
 */
function include_field_types_google_font_selector( $version ) {
	include_once('acf-google_font_selector-v5.php');
}

add_action('acf/register_fields', 'register_fields_google_font_selector');
/**
 * ACF 4 Field
 *
 * Loads the field for ACF 4
 *
 * @author Daniel Pataki
 * @since 3.0.0
 *
 */
function register_fields_google_font_selector() {
	include_once('acf-google_font_selector-v4.php');
}

add_action( 'admin_notices', 'acfgfs_setup_nag' );
/**
 * API Key Nag
 *
 * Displays a message promting users to supply the Google API key. It can be
 * added in the settings page, or it can be defined as a constant.
 *
 * @author Daniel Pataki
 * @since 3.0.0
 *
 */
function acfgfs_setup_nag() {
	if( function_exists('get_field') && !defined( 'GOOGLE_API_KEY' ) && !get_field('google_api_key', 'option') ) :
    ?>
    <div class="update-nag">
        <p><?php echo sprintf( __( 'The Google Font Selector Field requires an Google API key to work. You can set your API key on the <a href="%s">Site Options page</a>. If you need help getting an API key <a href="%s">click here</a>', 'acf-google-font-selector-field' ), admin_url( 'admin.php?page=site-options' ), 'https://wordpress.org/plugins/acf-google-font-selector-field/'  ); ?></p>
    </div>
    <?php
	endif;
}